
package com.sample.topgithub.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;

import com.sample.topgithub.R;
import com.sample.topgithub.fragment.RepositoryDetailFragment;
import com.sample.topgithub.fragment.RepositoryListFragment;
import com.sample.topgithub.model.Repository;
import com.sample.topgithub.presenter.GitActivityPresenter;
import com.sample.topgithub.util.DialogUtil;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(PowerMockRunner.class)
@PrepareForTest({GithubActivity.class, DialogUtil.class, RepositoryDetailFragment.class})
public class GithubActivityTest {

    private TestActivity testGithubActivity;

    @Before
    public void setup() {
        testGithubActivity = new TestActivity();
        Assert.assertNotNull(testGithubActivity);
        testGithubActivity.repositoryListFragment = PowerMockito.mock(RepositoryListFragment.class);
        testGithubActivity.repositoryDetailFragment = PowerMockito.mock(RepositoryDetailFragment.class);
        testGithubActivity.presenter = PowerMockito.mock(GitActivityPresenter.class);
    }

    @Test
    public void onFailure() {
        TestActivity spyTestActivity = PowerMockito.spy(testGithubActivity);
        PowerMockito.doReturn("test").when(spyTestActivity).getString(anyInt());
        PowerMockito.mockStatic(DialogUtil.class);
        spyTestActivity.isFinishing = true;
        spyTestActivity.onFailure("Test Message");
        PowerMockito.verifyStatic(times(0));
        DialogUtil.showSingleButtonDialog(any(Context.class), anyString()
                , anyString(), anyString(), any(DialogInterface.OnClickListener.class));
        spyTestActivity.isFinishing = false;
        spyTestActivity.onFailure("Test Message");
        PowerMockito.verifyStatic(times(1));
        DialogUtil.showSingleButtonDialog(any(Context.class), anyString()
                , anyString(), anyString(), any(DialogInterface.OnClickListener.class));
    }

    @Test
    public void onRepositoryListSuccess() {
        List list = new ArrayList<Repository>();
        testGithubActivity.isFinishing = true;
        testGithubActivity.onRepositoryListSuccess(list);
        verify(testGithubActivity.repositoryListFragment, times(0)).showRepositories(list);
        testGithubActivity.isFinishing = false;
        testGithubActivity.onRepositoryListSuccess(list);
        verify(testGithubActivity.repositoryListFragment).showRepositories(list);
    }

    @Test
    public void loadRepositoryList() {
        testGithubActivity.loadRepositoryList();
        verify(testGithubActivity.presenter).fetchRepositoryList();
    }

    @Test
    public void showRepositoryDetails() {
        PowerMockito.mockStatic(RepositoryDetailFragment.class);
        TestActivity spyTestActivity = PowerMockito.spy(testGithubActivity);
        PowerMockito.doNothing().when(spyTestActivity).addFragment(anyInt(),
                any(Fragment.class), anyString(), anyBoolean());
        Repository repository = PowerMockito.mock(Repository.class);
        spyTestActivity.showRepositoryDetails(repository);
        PowerMockito.verifyStatic();
        RepositoryDetailFragment.getInstance(repository);
        verify(spyTestActivity).addFragment(R.id.content,
                spyTestActivity.repositoryDetailFragment, "RepositoryDetailFragment", true);

    }

    @Test
    public void showProgress() {
        testGithubActivity.showProgress();
        verify(testGithubActivity.repositoryListFragment).showProgress();
    }

    @Test
    public void hideProgress() {
        testGithubActivity.hideProgress();
        verify(testGithubActivity.repositoryListFragment).hideProgress();
    }

    @Test
    public void onDestroy() {
        PowerMockito.suppress(MemberMatcher.method(AppCompatActivity.class, "onDestroy"));
        testGithubActivity.onDestroy();
        verify(testGithubActivity.presenter).destroy();
    }

    class TestActivity extends GithubActivity {
       public boolean isFinishing;

        @Override
        public boolean isFinishing() {
            return isFinishing;
        }
    }

}