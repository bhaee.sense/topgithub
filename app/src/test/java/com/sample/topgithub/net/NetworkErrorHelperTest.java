package com.sample.topgithub.net;

import android.support.annotation.Nullable;

import com.google.gson.Gson;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import retrofit2.adapter.rxjava.HttpException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({NetworkErrorHelper.class, HttpException.class, retrofit2.Response.class, Gson.class, ResponseBody.class})
public class NetworkErrorHelperTest {

    NetworkErrorHelper errorHelper;

    @Test
    public void getErrorMessage() {
        Exception exception = PowerMockito.mock(IOException.class);
        errorHelper = new NetworkErrorHelper(exception);
        Assert.assertEquals(NetworkErrorHelper.NO_INTERNET, errorHelper.getErrorMessage());
        exception = PowerMockito.mock(NullPointerException.class);
        errorHelper = new NetworkErrorHelper(exception);
        Assert.assertEquals(NetworkErrorHelper.ERROR_MESSAGE, errorHelper.getErrorMessage());
        HttpException httpException = PowerMockito.mock( HttpException.class);
        PowerMockito.when(exception.getMessage()).thenReturn("Test Error");
        errorHelper = new NetworkErrorHelper(httpException);
        Assert.assertEquals(NetworkErrorHelper.ERROR_MESSAGE, errorHelper.getErrorMessage());
    }

    @Test
    public void getMessage() {
        Exception exception = PowerMockito.mock(IOException.class);
        PowerMockito.when(exception.getMessage()).thenReturn("Test Error");
        errorHelper = new NetworkErrorHelper(exception);
        Assert.assertEquals("Test Error", errorHelper.getMessage());
    }

    @Test
    public void getJsonStringFromResponse() throws Exception {
        Exception exception = PowerMockito.mock(HttpException.class);
        errorHelper = new NetworkErrorHelper(exception);
        retrofit2.Response<?> response = PowerMockito.mock(retrofit2.Response.class);
        TestResponseBody responseBody = PowerMockito.mock(TestResponseBody.class);
        PowerMockito.when(responseBody.string()).thenReturn("{\"status\": \"404\"}");
        PowerMockito.when(response.errorBody()).thenReturn(responseBody);
        Assert.assertEquals("404", errorHelper.getJsonStringFromResponse(response));

    }

    class TestResponseBody extends ResponseBody {

        @Nullable
        @Override
        public MediaType contentType() {
            return null;
        }

        @Override
        public long contentLength() {
            return 0;
        }

        @Override
        public BufferedSource source() {
            return null;
        }

        @Override
        public String toString() {
            return "{\"status\": \"404\"}";
        }
    }


}