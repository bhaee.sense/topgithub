package com.sample.topgithub.util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.Serializable;
import java.util.Locale;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(PowerMockRunner.class)
public class GsonUtilTest {

    private static final String VALID_INPUT = "{ \"intVal\": 123, \"stringVal\": \"str\", \"longVal\": 456 }";
    private static final String EMPTY_STRING_INPUT = "{ \"intVal\": \"\",  \"intObjVal\": \"\",\"stringVal\": \"str\", \"longVal\": \"\", \"longObjVal\": \"\", \"floatVal\": \"\", \"floatObjVal\": \"\", \"doubleVal\": \"\", \"doubleObjVal\": \"\" }";
    private static final String HTML_INPUT = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\"> <html> <head> <meta http-equiv=\"Content-Type\" #content=\"text/html; charset=UTF-8\">";

    @Before
    public void setup() {
    }

    @Test
    public void toJson_nullInput() {
        assertNull(GsonUtil.toJson(null));
    }

    @Test
    public void parseValidJson() {
        final GsonTestClass output = GsonUtil.fromJson(VALID_INPUT, GsonTestClass.class, "");
        assertNotNull(output);
        assertEquals(123, output.intVal);
        assertEquals("str", output.stringVal);
        assertEquals(456, output.longVal);
    }

    @Test
    public void parseEmptyStringJson() {
        final GsonTestClass output = GsonUtil.fromJson(EMPTY_STRING_INPUT, GsonTestClass.class, "");
        assertNotNull(output);
        assertEquals(0, output.intVal);
        assertEquals(0, output.intObjVal.intValue());
        assertEquals(0, output.longVal);
        assertEquals(0, output.longObjVal.longValue());
        assertEquals(0.0f, output.floatVal, 0.0f);
        assertEquals(0.0f, output.floatObjVal, 0.0f);
        assertEquals(0.0d, output.doubleObjVal, 0.0d);
        assertEquals(0.0d, output.doubleObjVal, 0.0d);
    }

    @Test
    public void parseHtml() {
        assertNull(GsonUtil.fromJson(HTML_INPUT, GsonTestClass.class, ""));
        PowerMockito.verifyStatic(times(0));
    }

    @SuppressWarnings("unused")
    private class GsonTestClass implements Serializable {
        private int intVal;
        private Integer intObjVal;
        private String stringVal;
        private boolean booleanVal;
        private long longVal;
        private Long longObjVal;
        private float floatVal;
        private Float floatObjVal;
        private double doubleVal;
        private Double doubleObjVal;
    }
}