package com.sample.topgithub.fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.sample.topgithub.model.Repo;
import com.sample.topgithub.model.Repository;
import com.sample.topgithub.util.GsonUtil;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

@RunWith(PowerMockRunner.class)
@PrepareForTest({RepositoryDetailFragment.class, GsonUtil.class, ButterKnife.class,
        Glide.class})
public class RepositoryDetailFragmentTest {

    RepositoryDetailFragment testDetailFragment;
    Bundle bundle;
    Repository repository;

    @Before
    public void setup() throws Exception {
        bundle = PowerMockito.mock(Bundle.class);
        PowerMockito.whenNew(Bundle.class).withNoArguments().thenReturn(bundle);
        repository = PowerMockito.mock(Repository.class);
        testDetailFragment = RepositoryDetailFragment.getInstance(repository);
        Assert.assertNotNull(testDetailFragment);
        Mockito.verify(bundle).putString(anyString(), anyString());
    }

    @Test
    public void onViewCreated() {
        PowerMockito.mockStatic(ButterKnife.class);
        View view = PowerMockito.mock(View.class);
        testDetailFragment.onViewCreated(view, null);
        PowerMockito.verifyStatic();
        ButterKnife.bind(testDetailFragment, view);
    }

    @Test
    public void onActivityCreated() {
        RepositoryDetailFragment spyFragment = PowerMockito.spy(testDetailFragment);
        PowerMockito.doReturn(bundle).when(spyFragment).getArguments();
        PowerMockito.doNothing().when(spyFragment).showDetails(any(Repository.class));
        spyFragment.onActivityCreated(null);
        Mockito.verify(spyFragment).getArguments();
        Mockito.verify(spyFragment).showDetails(any(Repository.class));
        PowerMockito.verifyStatic();
        GsonUtil.fromJson(bundle.getString("repository_detail"), Repository.class, null);
    }

    @Test
    public void showDetails() {
        RepositoryDetailFragment spyFragment = PowerMockito.spy(testDetailFragment);
        spyFragment.nameTextView = PowerMockito.mock(TextView.class);
        spyFragment.usernameTextView = PowerMockito.mock(TextView.class);
        spyFragment.urlTextView = PowerMockito.mock(TextView.class);
        spyFragment.repoNameTextView = PowerMockito.mock(TextView.class);
        spyFragment.repoUrlTextView = PowerMockito.mock(TextView.class);
        spyFragment.descriptionTextView = PowerMockito.mock(TextView.class);
        spyFragment.imageView = PowerMockito.mock(ImageView.class);
        Context context = PowerMockito.mock(Context.class);
        PowerMockito.doReturn(context).when(spyFragment).getContext();
        PowerMockito.mockStatic(Glide.class);
        RequestManager requestManager = PowerMockito.mock(RequestManager.class);
        RequestBuilder<Drawable> builder = PowerMockito.mock(RequestBuilder.class);
        PowerMockito.when(Glide.with(any(Context.class))).thenReturn(requestManager);
        PowerMockito.doReturn(builder).when(requestManager).load(anyString());
        Repository repository = new Repository();
        repository.setName("Test Name");
        repository.setUsername("Test Username");
        repository.setAvatar("https://test.jpg");
        Repo repo = new Repo();
        repo.setName("Test Repo Name");
        repo.setUrl("Test Repo Url");
        repo.setDescription("Test Description");
        repository.setRepo(repo);
        spyFragment.showDetails(repository);
        Mockito.verify(spyFragment).getContext();
        Mockito.verify(requestManager).load(anyString());
        Mockito.verify(spyFragment.nameTextView).setText("Test Name");
        Mockito.verify(spyFragment.usernameTextView).setText("Test Username");
        Mockito.verify(spyFragment.repoNameTextView).setText("Test Repo Name");
        Mockito.verify(spyFragment.repoUrlTextView).setText("Test Repo Url");
        Mockito.verify(spyFragment.descriptionTextView).setText("Test Description");
    }

    @Test
    public void onDestroyView() {
        Unbinder unbinder = PowerMockito.mock(Unbinder.class);
        testDetailFragment.mUnbinder = null;
        testDetailFragment.onDestroyView();
        Mockito.verify(unbinder, Mockito.times(0)).unbind();
        testDetailFragment.mUnbinder = unbinder;
        testDetailFragment.onDestroyView();
        Mockito.verify(unbinder, Mockito.times(1)).unbind();
        Assert.assertNull( testDetailFragment.mUnbinder);
    }
}