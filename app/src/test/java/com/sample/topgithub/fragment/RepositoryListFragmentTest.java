package com.sample.topgithub.fragment;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.sample.topgithub.activity.GithubActivity;
import com.sample.topgithub.adapter.RepositoryListAdapter;
import com.sample.topgithub.fragmentListener.RepositoryListFragmentListener;
import com.sample.topgithub.model.Repository;
import com.sample.topgithub.util.ListenerUtil;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;

@RunWith(PowerMockRunner.class)
@PrepareForTest({RepositoryListFragment.class, ButterKnife.class,
        ListenerUtil.class})
public class RepositoryListFragmentTest {

    RepositoryListFragment testFragment;

    @Before
    public void setup() {
        testFragment = RepositoryListFragment.getInstance();
        Assert.assertNotNull(testFragment);
        testFragment.progressBar = PowerMockito.mock(ProgressBar.class);
    }

    @Test
    public void onViewCreated() {
        PowerMockito.mockStatic(ButterKnife.class);
        View view = PowerMockito.mock(View.class);
        testFragment.onViewCreated(view, null);
        PowerMockito.verifyStatic();
        ButterKnife.bind(testFragment, view);
    }

    @Test
    public void onActivityCreated() {
        RepositoryListFragment spyFragment = PowerMockito.spy(testFragment);
        GithubActivity activity = PowerMockito.mock(GithubActivity.class);
        PowerMockito.when(spyFragment.getActivity()).thenReturn(activity);
        PowerMockito.doNothing().when(spyFragment).initView();
        PowerMockito.mockStatic(ListenerUtil.class);
        RepositoryListFragmentListener fragmentListener = PowerMockito.mock(RepositoryListFragmentListener.class);
        PowerMockito.when(ListenerUtil.getListenerFromObject(activity, RepositoryListFragmentListener.class)).thenReturn(fragmentListener);
        spyFragment.onActivityCreated(null);
        Mockito.verify(spyFragment).initView();
        Mockito.verify(fragmentListener).loadRepositoryList();
        PowerMockito.verifyStatic();
        ListenerUtil.getListenerFromObject(activity, RepositoryListFragmentListener.class);

    }

    @Test
    public void initView() throws Exception {
        RepositoryListFragment spyFragment = PowerMockito.spy(testFragment);
        spyFragment.recyclerView = PowerMockito.mock(RecyclerView.class);
        GithubActivity activity = PowerMockito.mock(GithubActivity.class);
        PowerMockito.when(spyFragment.getActivity()).thenReturn(activity);
        Context context = PowerMockito.mock(GithubActivity.class);
        PowerMockito.when(spyFragment.getContext()).thenReturn(context);
        LinearLayoutManager linearLayoutManager = PowerMockito.mock(LinearLayoutManager.class);
        PowerMockito.whenNew(LinearLayoutManager.class).withArguments(context).thenReturn(linearLayoutManager);
        DividerItemDecoration itemDecoration = PowerMockito.mock(DividerItemDecoration.class);
        PowerMockito.whenNew(DividerItemDecoration.class).withArguments(activity, DividerItemDecoration.VERTICAL).thenReturn(itemDecoration);
        spyFragment.initView();
        Mockito.verify(spyFragment.recyclerView).setLayoutManager(any(LinearLayoutManager.class));
        Mockito.verify(spyFragment.recyclerView).addItemDecoration(any(DividerItemDecoration.class));
    }

    @Test
    public void showProgress() {
        testFragment.showProgress();
        Mockito.verify(testFragment.progressBar).setVisibility(View.VISIBLE);
    }

    @Test
    public void hideProgress() {
        testFragment.hideProgress();
        Mockito.verify(testFragment.progressBar).setVisibility(View.GONE);
    }

    @Test
    public void showRepositories() throws Exception {
        List<Repository> repositoryList = new ArrayList<>();
        RepositoryListFragment spyFragment = PowerMockito.spy(testFragment);
        Context context = PowerMockito.mock(GithubActivity.class);
        PowerMockito.when(spyFragment.getContext()).thenReturn(context);
        RepositoryListAdapter listAdapter = PowerMockito.mock(RepositoryListAdapter.class);
        PowerMockito.whenNew(RepositoryListAdapter.class).withArguments(any(Context.class), anyList(), any(RepositoryListAdapter.class))
                .thenReturn(listAdapter);
        spyFragment.recyclerView = PowerMockito.mock(RecyclerView.class);
        spyFragment.showRepositories(repositoryList);
        Mockito.verify(spyFragment.recyclerView).setAdapter(listAdapter);

    }

    @Test
    public void onDestroyView() {
        Unbinder unbinder = PowerMockito.mock(Unbinder.class);
        RecyclerView recyclerView = PowerMockito.mock(RecyclerView.class);
        testFragment.mUnbinder = null;
        testFragment.onDestroyView();
        Mockito.verify(unbinder, Mockito.times(0)).unbind();
        Mockito.verify(recyclerView, Mockito.times(0)).setAdapter(null);
        testFragment.mUnbinder = unbinder;
        testFragment.recyclerView = recyclerView;
        testFragment.onDestroyView();
        Mockito.verify(unbinder, Mockito.times(1)).unbind();
        Mockito.verify(recyclerView, Mockito.times(1)).setAdapter(null);
        Assert.assertNull(testFragment.mUnbinder);
    }
}