package com.sample.topgithub.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.sample.topgithub.R;
import com.sample.topgithub.activity.GithubActivity;
import com.sample.topgithub.activity.GithubActivityTest;
import com.sample.topgithub.fragment.RepositoryDetailFragment;
import com.sample.topgithub.fragment.RepositoryListFragment;
import com.sample.topgithub.model.Repository;
import com.sample.topgithub.presenter.GitActivityPresenter;
import com.sample.topgithub.util.DialogUtil;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({RepositoryListAdapter.class, LayoutInflater.class, Glide.class})
public class RepositoryListAdapterTest {

    private static final int TEST_VIEW_TYPE = 0;
    RepositoryListAdapter testAdapter;
    private RepositoryListAdapter.OnItemClickListener onItemClickListener = PowerMockito.mock(RepositoryListAdapter.OnItemClickListener.class);
    private List<Repository> repositories;
    private Context context = PowerMockito.mock(Context.class);

    protected LayoutInflater mockLayoutInflater = PowerMockito.mock(LayoutInflater.class);

    protected ViewGroup mockParent = PowerMockito.mock(ViewGroup.class);

    protected Context mockContext = PowerMockito.mock(Context.class);

    protected View mockInflatedView = PowerMockito.mock(View.class);

    @Before
    public void setup() {
        repositories = getItems();
        testAdapter = new RepositoryListAdapter(context, repositories, onItemClickListener);
        Assert.assertNotNull(testAdapter);
    }

    protected void setupCreateViewHolderTest(final int layoutRes) {
        PowerMockito.mockStatic(LayoutInflater.class);
        PowerMockito.when(LayoutInflater.from(mockContext)).thenReturn(mockLayoutInflater);
        PowerMockito.when(mockParent.getContext()).thenReturn(mockContext);
        PowerMockito.when(mockLayoutInflater.inflate(layoutRes, null)).thenReturn(mockInflatedView);
    }

    protected void validateCreatedViewHolder(@NonNull final RecyclerView.ViewHolder vh) {
        verify(mockInflatedView).setLayoutParams(any(RecyclerView.LayoutParams.class));
    }

    @Test
    public void onCreateViewHolder() {
        setupCreateViewHolderTest(R.layout.repository_list_item);
        validateCreatedViewHolder(testAdapter.onCreateViewHolder(mockParent, TEST_VIEW_TYPE));
    }

    @Test
    public void onBindViewHolder() {
        PowerMockito.mockStatic(Glide.class);
        RequestManager requestManager = PowerMockito.mock(RequestManager.class);
        RequestBuilder<Drawable> builder = PowerMockito.mock(RequestBuilder.class);
        PowerMockito.when(Glide.with(any(Context.class))).thenReturn(requestManager);
        PowerMockito.doReturn(builder).when(requestManager).load(anyString());
        RepositoryListAdapter.ViewHolder holder = new RepositoryListAdapter.ViewHolder(mockInflatedView);
        holder.nameTextView = PowerMockito.mock(TextView.class);
        holder.userNameTextView = PowerMockito.mock(TextView.class);
        testAdapter.onBindViewHolder(holder, 0);
        verify(holder.nameTextView).setText("Microsoft");
        verify(holder.userNameTextView).setText("microsoft");
        verify(requestManager).load("https://microsoft.jpg");
        testAdapter.onBindViewHolder(holder, 1);
        verify(holder.nameTextView).setText("Google");
        verify(holder.userNameTextView).setText("google");
        verify(requestManager).load("https://google.jpg");

    }

    @Test
    public void getItemCount() {
        Assert.assertEquals(2, testAdapter.getItemCount());
    }

    private List<Repository> getItems() {
        Repository repository1 = new Repository();
        repository1.setName("Microsoft");
        repository1.setUsername("microsoft");
        repository1.setAvatar("https://microsoft.jpg");
        Repository repository2 = new Repository();
        repository2.setName("Google");
        repository2.setUsername("google");
        repository2.setAvatar("https://google.jpg");
        return Arrays.asList(repository1, repository2);
    }
}