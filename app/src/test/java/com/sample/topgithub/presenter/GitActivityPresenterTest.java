package com.sample.topgithub.presenter;
import com.sample.topgithub.model.Repository;
import com.sample.topgithub.net.RXHelper;
import com.sample.topgithub.presenterlistener.GitActivityPresenterListener;
import com.sample.topgithub.net.NetworkErrorHelper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(PowerMockRunner.class)
@PrepareForTest({GitActivityPresenter.class, CompositeSubscription.class})
public class GitActivityPresenterTest {

    GitActivityPresenter testPresenter;
    RXHelper rxHelper;
    GitActivityPresenterListener presenterListener;

    @Before
    public void setup() {
        rxHelper = PowerMockito.mock(RXHelper.class);
        presenterListener = PowerMockito.mock(GitActivityPresenterListener.class);
        testPresenter = new GitActivityPresenter(rxHelper, presenterListener);
        Assert.assertNotNull(testPresenter);
        testPresenter.subscriptions = PowerMockito.mock(CompositeSubscription.class);
    }

    @Test
    public void getRepositoryList() {
        List<Repository> repositories = new ArrayList<>();
        Repository repository = PowerMockito.mock(Repository.class);
        ArgumentCaptor<RXHelper.RepositoryListCallback> captor =
                ArgumentCaptor.forClass(RXHelper.RepositoryListCallback.class);
        Subscription subscription = PowerMockito.mock(Subscription.class);
        PowerMockito.doReturn(subscription).when(rxHelper).getRepositoryList(captor.capture());
        testPresenter.fetchRepositoryList();
        captor.getValue().onSuccess(repositories);
        Mockito.verify(presenterListener).showProgress();
        Mockito.verify(testPresenter.subscriptions).add(any(Subscription.class));
        Mockito.verify(presenterListener).hideProgress();
        Mockito.verify(presenterListener, times(0)).onRepositoryListSuccess(repositories);
        Mockito.verify(presenterListener, times(1)).showEmptyRepositoryList();
        repositories.add(repository);
        testPresenter.fetchRepositoryList();
        captor.getValue().onSuccess(repositories);
        NetworkErrorHelper networkError = PowerMockito.mock(NetworkErrorHelper.class);
        testPresenter.fetchRepositoryList();
        captor.getValue().onError(networkError);
        Mockito.verify(presenterListener, Mockito.times(3)).hideProgress();
        Mockito.verify(presenterListener).onFailure(anyString());
    }

    @Test
    public void destroy() {
        testPresenter.destroy();
        verify(testPresenter.subscriptions).unsubscribe();
    }
}