package com.sample.topgithub.activity;

import android.content.Intent;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.sample.topgithub.MockServerDispatcher;
import com.sample.topgithub.R;
import com.sample.topgithub.TestNetModule;
import com.sample.topgithub.recyclerview.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
public class GithubActivityTest {

    @Rule
    public ActivityTestRule<GithubActivity> activityRule =
            new ActivityTestRule<>(GithubActivity.class,false,false);

    private MockWebServer webServer;
    private MockServerDispatcher.RequestDispatcher requestDispatcher;

    @Before
    public void setup() throws Exception {
        requestDispatcher = new MockServerDispatcher().new RequestDispatcher();
        webServer = new MockWebServer();
        webServer.setDispatcher(requestDispatcher);
        webServer.start(8080);
    }

    @After
    public void tearDown() throws Exception {
        webServer.shutdown();
    }

    @Test
    public void testSuccessCondition() {
        requestDispatcher.setTestResponseCode(200);
        activityRule.launchActivity(new Intent());
        Espresso.onView(withId(R.id.progressbar)).check(matches(not((isDisplayed()))));
        Espresso.onView(withId(R.id.recyclerview)).check(matches((isDisplayed())));
        testDataDisplayedOnRecyclerView();
    }

    public void testDataDisplayedOnRecyclerView() {
        Espresso.onView(withId(R.id.recyclerview))
                .perform(RecyclerViewActions.scrollToPosition(0));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(0, R.id.name_textview))
                .check(matches(withText("Microsoft")));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(0, R.id.username_textview))
                .check(matches(withText("Microsoft")));
        Espresso.onView(withId(R.id.recyclerview))
                .perform(RecyclerViewActions.scrollToPosition(1));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(1, R.id.name_textview))
                .check(matches(withText("Google")));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(1, R.id.username_textview))
                .check(matches(withText("google")));
        Espresso.onView(withId(R.id.recyclerview))
                .perform(RecyclerViewActions.scrollToPosition(2));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(2, R.id.name_textview))
                .check(matches(withText("Facebook")));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(2, R.id.username_textview))
                .check(matches(withText("facebook")));
        Espresso.onView(withId(R.id.recyclerview))
                .perform(RecyclerViewActions.scrollToPosition(3));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(3, R.id.name_textview))
                .check(matches(withText("Alibaba")));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(3, R.id.username_textview))
                .check(matches(withText("alibaba")));
        Espresso.onView(withId(R.id.recyclerview))
                .perform(RecyclerViewActions.scrollToPosition(4));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(4, R.id.name_textview))
                .check(matches(withText("The Apache Software Foundation")));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(4, R.id.username_textview))
                .check(matches(withText("apache")));
        Espresso.onView(withId(R.id.recyclerview))
                .perform(RecyclerViewActions.scrollToPosition(5));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(5, R.id.name_textview))
                .check(matches(withText("FOSSASIA")));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(5, R.id.username_textview))
                .check(matches(withText("fossasia")));
        Espresso.onView(withId(R.id.recyclerview))
                .perform(RecyclerViewActions.scrollToPosition(6));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(6, R.id.name_textview))
                .check(matches(withText("SnailClimb")));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(6, R.id.username_textview))
                .check(matches(withText("Snailclimb")));
        Espresso.onView(withId(R.id.recyclerview))
                .perform(RecyclerViewActions.scrollToPosition(7));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(7, R.id.name_textview))
                .check(matches(withText("Tencent")));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(7, R.id.username_textview))
                .check(matches(withText("Tencent")));
        Espresso.onView(withId(R.id.recyclerview))
                .perform(RecyclerViewActions.scrollToPosition(8));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(8, R.id.name_textview))
                .check(matches(withText("Airbnb")));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(8, R.id.username_textview))
                .check(matches(withText("airbnb")));
        Espresso.onView(withId(R.id.recyclerview))
                .perform(RecyclerViewActions.scrollToPosition(9));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(9, R.id.name_textview))
                .check(matches(withText("Flutter")));
        Espresso.onView(TestUtils.withRecyclerView(R.id.recyclerview)
                .atPositionOnView(9, R.id.username_textview))
                .check(matches(withText("flutter")));
        testRecyclerItemClick();
    }

    public void testRecyclerItemClick() {
        Espresso.onView(withId(R.id.recyclerview))
                .perform(RecyclerViewActions.scrollToPosition(9));
        Espresso.onView(withId(R.id.recyclerview))
                .perform(RecyclerViewActions.actionOnItemAtPosition(9, click()));
        Espresso.onView(withId(R.id.name_value_textview)).check(matches(withText("Flutter")));
        Espresso.onView(withId(R.id.username_value_textview)).check(matches(withText("flutter")));
        Espresso.onView(withId(R.id.description_value_textview)).check(matches(withText("Flutter makes it easy and fast to build beautiful mobile apps.")));
        Espresso.onView(withId(R.id.repo_name_value_textview)).check(matches(withText("flutter-intellij")));
        Espresso.onView(withId(R.id.repo_url_value_textview)).check(matches(withText("https://github.com/flutter/flutter-intellij")));
        Espresso.onView(withId(R.id.url_value_textview)).check(matches(withText("https://github.com/flutter")));
    }

    @Test
    public void testResultNotFoundCondition() {
        requestDispatcher.setTestResponseCode(404);
        activityRule.launchActivity(new Intent());
        Espresso.onView(withText("File Not Found")).check(matches(isDisplayed()));
        Espresso.onView(withText("Oops!")).check(matches(isDisplayed()));
        Espresso.onView(withId(android.R.id.button1)).perform(click());

    }

    @Test
    public void testBadRequestCondition() {
        requestDispatcher.setTestResponseCode(400);
        activityRule.launchActivity(new Intent());
        Espresso.onView(withText("Bad Request")).check(matches(isDisplayed()));
        Espresso.onView(withText("Oops!")).check(matches(isDisplayed()));
        Espresso.onView(withId(android.R.id.button1)).perform(click());
    }

    @Test
    public void testServerErrorCondition() {
        requestDispatcher.setTestResponseCode(500);
        activityRule.launchActivity(new Intent());
        Espresso.onView(withText("Server Error")).check(matches(isDisplayed()));
        Espresso.onView(withText("Oops!")).check(matches(isDisplayed()));
        Espresso.onView(withId(android.R.id.button1)).perform(click());
    }
}