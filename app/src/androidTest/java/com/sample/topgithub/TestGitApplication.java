package com.sample.topgithub;

import java.io.File;

public class TestGitApplication extends GitApplication {

    @Override
    public GitComponent getGitComponent() {

        return DaggerGitComponent.builder()
                .netModule(new TestNetModule(new File(getCacheDir(), "data")))
                .build();
    }
}