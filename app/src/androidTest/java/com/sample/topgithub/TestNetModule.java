package com.sample.topgithub;

import com.sample.topgithub.net.NetModule;

import java.io.File;
import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Module
public class TestNetModule extends NetModule {

    public TestNetModule(File cacheFile) {
        super(cacheFile);
    }

    public String getBaseUrl() {
        return "http://localhost:8080/";
    }


}
