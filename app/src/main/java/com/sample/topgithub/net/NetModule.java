
package com.sample.topgithub.net;

import com.sample.topgithub.BuildConfig;

import java.io.File;
import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Module
public class NetModule {

    protected static final int CACHE_TIME = 24 * 60 * 60;
    protected static final int CACHE_SIZE = 5 * 1024 * 1024;

    protected File cacheFile;

    public NetModule(File cacheFile) {
        this.cacheFile = cacheFile;
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        Cache cache = null;
        try {
            cache = new Cache(cacheFile, CACHE_SIZE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        Request request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("Cache-Control", String.format("max-age=%d", CACHE_TIME))
                                .build();

                        okhttp3.Response response = chain.proceed(request);
                        response.cacheResponse();
                        return response;
                    }
                })
                .cache(cache)

                .build();


        return new Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())

                .build();
    }

    public String getBaseUrl() {
        return BuildConfig.BASE_URL;
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public APIInterface providesAPIInterface(
            Retrofit retrofit) {
        return retrofit.create(APIInterface.class);
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public RXHelper providesService(
            APIInterface networkService) {
        return new RXHelper(networkService);
    }

}
