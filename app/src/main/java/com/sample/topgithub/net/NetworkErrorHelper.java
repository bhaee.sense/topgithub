
package com.sample.topgithub.net;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import retrofit2.adapter.rxjava.HttpException;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class NetworkErrorHelper extends Throwable {

    public static final String NO_INTERNET = "No Internet!";
    public static final String ERROR_MESSAGE = "Something went wrong. Please try again.";
    public static final String ERROR_HEADER = "Error-Message";

    private final Throwable error;

    public NetworkErrorHelper(Throwable e) {
        super(e);
        this.error = e;
    }

    public String getMessage() {
        return error.getMessage();
    }


    public String getErrorMessage() {
        if (this.error instanceof IOException) return NO_INTERNET;
        if (!(this.error instanceof HttpException)) return ERROR_MESSAGE;
        retrofit2.Response<?> response = ((HttpException) this.error).response();
        if (response != null) {
            String status = getJsonStringFromResponse(response);
            if (!TextUtils.isEmpty(status)) return status;

            Map<String, List<String>> headers = response.headers().toMultimap();
            if (headers.containsKey(ERROR_HEADER))
                return headers.get(ERROR_HEADER).get(0);
        }

        return ERROR_MESSAGE;
    }

    protected String getJsonStringFromResponse(final retrofit2.Response<?> response) {
        try {
            String jsonString = response.errorBody().string();
            Response errorResponse = new Gson().fromJson(jsonString, Response.class);
            return errorResponse.status;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        NetworkErrorHelper that = (NetworkErrorHelper) object;

        return error != null ? error.equals(that.error) : that.error == null;

    }

    @Override
    public int hashCode() {
        return error != null ? error.hashCode() : 0;
    }
}
