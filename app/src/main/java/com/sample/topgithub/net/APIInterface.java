
package com.sample.topgithub.net;

import com.sample.topgithub.model.Repository;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface APIInterface {

    @GET("developers?language=java&amp;since=weekly")
    Observable<List<Repository>> getRepositoryList();
}
