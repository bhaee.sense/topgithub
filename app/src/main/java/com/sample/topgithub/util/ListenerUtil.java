
package com.sample.topgithub.util;

import com.sample.topgithub.exception.NoListenerImplementedException;

public final class ListenerUtil {

    private ListenerUtil() {
        // hide constructor
    }

    public static <T> T getListenerFromObject(Object caller, Class<T> classOfT) {
        final T retObj;
        if (!classOfT.isAssignableFrom(caller.getClass())) {
            throw new NoListenerImplementedException(caller.getClass().getSimpleName() +
                    " must implement " + classOfT.getSimpleName());
        } else {
            retObj = (T) caller;
        }
        return retObj;
    }
}
