
package com.sample.topgithub.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.WindowManager;

public class DialogUtil {

    public static void showSingleButtonDialog(final Context context
            , final String title
            , final String message
            , final String buttonTitle
            , DialogInterface.OnClickListener onClickListener) {
        try {
            AlertDialog dialog = new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton(buttonTitle, onClickListener).create();
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }

    }
}
