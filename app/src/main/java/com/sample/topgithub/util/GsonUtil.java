
package com.sample.topgithub.util;
import android.support.annotation.Nullable;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;

@SuppressWarnings("WeakerAccess")
public final class GsonUtil {
    private static final String TAG = GsonUtil.class.getName();
    private static Gson sGson;

    static {
        final IntTypeAdapter sIntTypeAdapter = new IntTypeAdapter();
        final LongTypeAdapter sLongTypeAdapter = new LongTypeAdapter();
        final FloatTypeAdapter sFloatTypeAdapter = new FloatTypeAdapter();
        final DoubleTypeAdapter sDoubleTypeAdapter = new DoubleTypeAdapter();
        sGson = new GsonBuilder()
                .registerTypeAdapter(int.class, sIntTypeAdapter)
                .registerTypeAdapter(Integer.class, sIntTypeAdapter)
                .registerTypeAdapter(long.class, sLongTypeAdapter)
                .registerTypeAdapter(Long.class, sLongTypeAdapter)
                .registerTypeAdapter(float.class, sFloatTypeAdapter)
                .registerTypeAdapter(Float.class, sFloatTypeAdapter)
                .registerTypeAdapter(double.class, sDoubleTypeAdapter)
                .registerTypeAdapter(Double.class, sDoubleTypeAdapter)
                .create();
    }

    private GsonUtil() {
        // hide constructor
    }

    public static <T> T fromJson(String json, Class<T> classOfT, String requestString) {
        return fromJson(json, true, classOfT, requestString);
    }

    public static <T> T fromJson(String json, boolean logException, Class<T> classOfT, String requestString) {
        T retObj;

        try {
            retObj = sGson.fromJson(json, classOfT);
        } catch (Exception e) {
            if (logException) {
            }
            retObj = null;
        }
        return retObj;
    }

    public static @Nullable
    String toJson(@Nullable final Object src) {
        if (src == null) {
            return null;
        }

        String jsonString;
        try {
            jsonString = sGson.toJson(src);
        } catch (Exception e) {
            jsonString = null;
        }
        return jsonString;

    }


    private static class IntTypeAdapter extends TypeAdapter<Integer> {

        @Override
        public void write(JsonWriter out, Integer value) throws IOException {
            if (value == null) {
                out.nullValue();
                return;
            }
            out.value(value);
        }

        @Override
        public Integer read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            final String stringVal = in.nextString();
            try {
                return Integer.valueOf(stringVal);
            } catch (NumberFormatException e) {
                // do nothing
            }
            return 0;
        }
    }

    private static class LongTypeAdapter extends TypeAdapter<Long> {

        @Override
        public void write(JsonWriter out, Long value) throws IOException {
            if (value == null) {
                out.nullValue();
                return;
            }
            out.value(value);
        }

        @Override
        public Long read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            final String stringVal = in.nextString();
            try {
                return Long.valueOf(stringVal);
            } catch (NumberFormatException e) {
                // do nothing
            }
            return 0L;
        }
    }

    private static class FloatTypeAdapter extends TypeAdapter<Float> {

        @Override
        public void write(JsonWriter out, Float value) throws IOException {
            if (value == null) {
                out.nullValue();
                return;
            }
            out.value(value);
        }

        @Override
        public Float read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            final String stringVal = in.nextString();
            try {
                return Float.valueOf(stringVal);
            } catch (NumberFormatException e) {
                // do nothing
            }
            return 0.0f;
        }
    }

    private static class DoubleTypeAdapter extends TypeAdapter<Double> {

        @Override
        public void write(JsonWriter out, Double value) throws IOException {
            if (value == null) {
                out.nullValue();
                return;
            }
            out.value(value);
        }

        @Override
        public Double read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            final String stringVal = in.nextString();
            try {
                return Double.valueOf(stringVal);
            } catch (NumberFormatException e) {
                // do nothing
            }
            return 0.0d;
        }
    }
}
