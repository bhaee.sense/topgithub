package com.sample.topgithub.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sample.topgithub.R;
import com.sample.topgithub.model.Repository;

import java.util.List;

public class RepositoryListAdapter extends RecyclerView.Adapter<RepositoryListAdapter.ViewHolder> {

    private final OnItemClickListener onItemClickListener;
    private List<Repository> repositories;
    private Context context;

    public RepositoryListAdapter(Context context, List<Repository> data, OnItemClickListener listener) {
        this.repositories = data;
        this.onItemClickListener = listener;
        this.context = context;
    }

    @Override
    public RepositoryListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repository_list_item, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RepositoryListAdapter.ViewHolder holder, int position) {
        holder.click(repositories.get(position), onItemClickListener);
        holder.nameTextView.setText(repositories.get(position).getName());
        holder.userNameTextView.setText(repositories.get(position).getUsername());
        String images = repositories.get(position).getAvatar();
        Glide.with(context)
                .load(images)
                .into(holder.background);

    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }


    public interface OnItemClickListener {
        void onClick(Repository Item);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView nameTextView, userNameTextView;
        ImageView background;

        public ViewHolder(View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.name_textview);
            userNameTextView = itemView.findViewById(R.id.username_textview);
            background = itemView.findViewById(R.id.imageView);

        }


        public void click(final Repository cityListData, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(cityListData);
                }
            });
        }
    }


}

