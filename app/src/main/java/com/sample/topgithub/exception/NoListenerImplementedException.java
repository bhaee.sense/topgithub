

package com.sample.topgithub.exception;


public class NoListenerImplementedException extends RuntimeException {

    public NoListenerImplementedException(String msg) {
        super(msg);
    }
}
