package com.sample.topgithub.activity;
import android.content.DialogInterface;
import android.os.Bundle;

import com.sample.topgithub.GitApplication;
import com.sample.topgithub.R;
import com.sample.topgithub.fragment.RepositoryDetailFragment;
import com.sample.topgithub.fragment.RepositoryListFragment;
import com.sample.topgithub.fragmentListener.RepositoryListFragmentListener;
import com.sample.topgithub.model.Repository;
import com.sample.topgithub.net.RXHelper;
import com.sample.topgithub.presenter.GitActivityPresenter;
import com.sample.topgithub.presenterlistener.GitActivityPresenterListener;
import com.sample.topgithub.util.DialogUtil;

import java.util.List;

import javax.inject.Inject;

public class GithubActivity extends BaseActivity implements GitActivityPresenterListener,
        RepositoryListFragmentListener {

    private static final String REPOSITORY_LIST_FRAGMENT = "RepositoryListFragment";
    private static final String REPOSITORY_DETAIL_FRAGMENT = "RepositoryDetailFragment";

    @Inject
    public RXHelper rxHelper;

    protected GitActivityPresenter presenter;
    protected RepositoryListFragment repositoryListFragment;
    protected RepositoryDetailFragment repositoryDetailFragment;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_github);
        ((GitApplication)getApplication()).getGitComponent().inject(this);
        repositoryListFragment = RepositoryListFragment.getInstance();
        addFragment(R.id.content, repositoryListFragment, REPOSITORY_LIST_FRAGMENT, false);
        presenter = new GitActivityPresenter(rxHelper, this);
    }

    @Override
    public void showProgress() {
        repositoryListFragment.showProgress();
    }

    @Override
    public void hideProgress() {
        repositoryListFragment.hideProgress();
    }

    @Override
    public void onFailure(String appErrorMessage) {
        if(!isFinishing()) {
            DialogUtil.showSingleButtonDialog(this, getString(R.string.error_title)
            , appErrorMessage, getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
        }
    }

    @Override
    public void onRepositoryListSuccess(List<Repository> repositoryList) {
        if(!isFinishing()) {
            repositoryListFragment.showRepositories(repositoryList);
        }
    }

    @Override
    public void showEmptyRepositoryList() {
        DialogUtil.showSingleButtonDialog(this, getString(R.string.error_title)
                , getString(R.string.empty_repository_list), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        return;
    }

    @Override
    public void loadRepositoryList() {
        presenter.fetchRepositoryList();
    }

    @Override
    public void showRepositoryDetails(Repository item) {
        repositoryDetailFragment = RepositoryDetailFragment.getInstance(item);
        addFragment(R.id.content, repositoryDetailFragment, REPOSITORY_DETAIL_FRAGMENT, true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }
}
