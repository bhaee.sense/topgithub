

package com.sample.topgithub.fragmentListener;

import com.sample.topgithub.model.Repository;

public interface RepositoryListFragmentListener {

    void loadRepositoryList();

    void showRepositoryDetails(Repository item);
}
