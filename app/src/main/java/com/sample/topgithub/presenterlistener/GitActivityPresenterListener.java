
package com.sample.topgithub.presenterlistener;

import com.sample.topgithub.model.Repository;

import java.util.List;

public interface GitActivityPresenterListener {

    void showProgress();

    void hideProgress();

    void onFailure(String appErrorMessage);

    void onRepositoryListSuccess(List<Repository> repositories);

    void showEmptyRepositoryList();
}
