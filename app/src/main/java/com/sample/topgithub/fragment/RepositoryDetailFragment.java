
package com.sample.topgithub.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sample.topgithub.R;
import com.sample.topgithub.model.Repository;
import com.sample.topgithub.util.GsonUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RepositoryDetailFragment extends Fragment {

    private static final String REPOSITORY_DETAIL = "repository_detail";

    @BindView(R.id.imageView)
    protected ImageView imageView;
    @BindView(R.id.name_value_textview)
    protected TextView nameTextView;
    @BindView(R.id.username_value_textview)
    protected TextView usernameTextView;
    @BindView(R.id.url_value_textview)
    protected TextView urlTextView;
    @BindView(R.id.repo_name_value_textview)
    protected TextView repoNameTextView;
    @BindView(R.id.description_value_textview)
    protected TextView descriptionTextView;
    @BindView(R.id.repo_url_value_textview)
    protected TextView repoUrlTextView;
    protected Unbinder mUnbinder;

    public static RepositoryDetailFragment getInstance(@NonNull Repository repository) {
        RepositoryDetailFragment repositoryDetailFragment = new RepositoryDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(REPOSITORY_DETAIL, GsonUtil.toJson(repository));
        repositoryDetailFragment.setArguments(bundle);
        return repositoryDetailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_repository_detail, null);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUnbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            Repository repository = GsonUtil.fromJson(bundle.getString(REPOSITORY_DETAIL), Repository.class, null);
            showDetails(repository);
        }
    }

    protected void showDetails(Repository repository) {
        Glide.with(getContext())
                .load(repository.getAvatar())
                .into(imageView);
        nameTextView.setText(repository.getName());
        usernameTextView.setText(repository.getUsername());
        urlTextView.setText(repository.getUrl());
        repoNameTextView.setText(repository.getRepo().getName());
        descriptionTextView.setText(repository.getRepo().getDescription());
        repoUrlTextView.setText(repository.getRepo().getUrl());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
    }

}
