
package com.sample.topgithub.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.sample.topgithub.R;
import com.sample.topgithub.adapter.RepositoryListAdapter;
import com.sample.topgithub.fragmentListener.RepositoryListFragmentListener;
import com.sample.topgithub.model.Repository;
import com.sample.topgithub.util.DialogUtil;
import com.sample.topgithub.util.ListenerUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RepositoryListFragment extends Fragment {

    private RepositoryListFragmentListener fragmentListener;

    @BindView(R.id.recyclerview)
    protected RecyclerView recyclerView;
    @BindView(R.id.progressbar)
    protected ProgressBar progressBar;

    protected Unbinder mUnbinder;

    public static RepositoryListFragment getInstance() {
        return new RepositoryListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_repository_list, null);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUnbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        fragmentListener = ListenerUtil.getListenerFromObject(getActivity(), RepositoryListFragmentListener.class);
        fragmentListener.loadRepositoryList();
    }

    protected void initView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new
                DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    public void showRepositories(List<Repository> repositoryList) {

        RepositoryListAdapter adapter = new RepositoryListAdapter(getContext(), repositoryList,
                new RepositoryListAdapter.OnItemClickListener() {
                    @Override
                    public void onClick(Repository item) {
                        fragmentListener.showRepositoryDetails(item);
                    }
                });

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }

        if(recyclerView != null) {
            recyclerView.setAdapter(null);
        }
    }
}
