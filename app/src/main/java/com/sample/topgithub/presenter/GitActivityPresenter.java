package com.sample.topgithub.presenter;

import com.sample.topgithub.model.Repository;
import com.sample.topgithub.net.NetworkErrorHelper;
import com.sample.topgithub.net.RXHelper;
import com.sample.topgithub.presenterlistener.GitActivityPresenterListener;

import java.util.List;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class GitActivityPresenter {

    private final RXHelper rxHelper;
    private final GitActivityPresenterListener presenterListener;
    protected CompositeSubscription subscriptions;

    public GitActivityPresenter(RXHelper rxHelper, GitActivityPresenterListener presenterListener) {
        this.rxHelper = rxHelper;
        this.presenterListener = presenterListener;
        this.subscriptions = new CompositeSubscription();
    }

    public void fetchRepositoryList() {
        presenterListener.showProgress();

        Subscription subscription = rxHelper.getRepositoryList(new RXHelper.RepositoryListCallback() {
            @Override
            public void onSuccess(List<Repository> cityListResponse) {
                presenterListener.hideProgress();
                if (cityListResponse != null && cityListResponse.size() > 0) {
                    presenterListener.onRepositoryListSuccess(cityListResponse);
                } else {
                    presenterListener.showEmptyRepositoryList();
                }

            }

            @Override
            public void onError(NetworkErrorHelper networkError) {
                presenterListener.hideProgress();
                presenterListener.onFailure(networkError.getErrorMessage());
            }

        });

        subscriptions.add(subscription);
    }

    public void destroy() {
        subscriptions.unsubscribe();
    }
}
