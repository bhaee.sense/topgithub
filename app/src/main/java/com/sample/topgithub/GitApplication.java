package com.sample.topgithub;

import android.app.Application;

import com.sample.topgithub.net.NetModule;

import java.io.File;

public class GitApplication extends Application {

    GitComponent gitComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        getGitComponent();
    }

    public GitComponent getGitComponent() {
        if (gitComponent == null) {
            File cacheFile = new File(getCacheDir(), "data");
            gitComponent = DaggerGitComponent.builder().netModule(new NetModule(cacheFile)).build();
        }
        return gitComponent;
    }
}
