
package com.sample.topgithub;

import com.sample.topgithub.activity.GithubActivity;
import com.sample.topgithub.net.NetModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetModule.class,})
public interface GitComponent {
    void inject(GithubActivity activity);
}
